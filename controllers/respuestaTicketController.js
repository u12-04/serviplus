const agregar = async (req, res) => {
    console.log("respondiedo desde el metodo crear");
}

const listar = async (req, res) => {
    console.log("respondiedo desde el metodo listar");
}

const eliminar = async (req, res) => {
    console.log("respondiedo desde el metodo eliminar");
}

const editar = async (req, res) => {
    console.log("respondiedo desde el metodo editar");
}

const listarUno = async (req, res) => {
    console.log("respondiedo desde el metodo listarUno");
}

export {
    agregar,
    listar,
    eliminar,
    editar,
    listarUno
}