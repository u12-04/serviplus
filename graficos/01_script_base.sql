/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     7/10/2022 1:46:41 p. m.                      */
/*==============================================================*/


/*==============================================================*/
/* Table: categorias                                            */
/*==============================================================*/
create table categorias
(
   id_categoria         bigint not null auto_increment  comment '',
   nombre_categoria     varchar(200) not null  comment '',
   primary key (id_categoria)
);

/*==============================================================*/
/* Table: imagenes_respuestas                                   */
/*==============================================================*/
create table imagenes_respuestas
(
   id_imagen_respuesta  bigint not null auto_increment  comment '',
   id_respuesta_ticket  bigint not null  comment '',
   nombre_publico_imagen varchar(200) not null  comment '',
   nombre_privado_imagen varchar(200) not null  comment '',
   primary key (id_imagen_respuesta)
);

/*==============================================================*/
/* Table: imagenes_tickets                                      */
/*==============================================================*/
create table imagenes_tickets
(
   id_imagen_ticket     bigint not null auto_increment  comment '',
   id_ticket            bigint not null  comment '',
   nombre_publico_imagen varchar(200) not null  comment '',
   nombre_privado_imagen varchar(200) not null  comment '',
   primary key (id_imagen_ticket)
);

/*==============================================================*/
/* Table: respuestas_tickets                                    */
/*==============================================================*/
create table respuestas_tickets
(
   id_respuesta_ticket  bigint not null auto_increment  comment '',
   id_ticket            bigint not null  comment '',
   id_usuario           bigint not null  comment '',
   fecha_respuesta_ticket date not null  comment '',
   hora_respuesta_ticket time not null  comment '',
   descripcion_ticket_respuesta text not null  comment '',
   primary key (id_respuesta_ticket)
);

/*==============================================================*/
/* Table: roles                                                 */
/*==============================================================*/
create table roles
(
   id_rol               bigint not null auto_increment  comment '',
   nombre_rol           varchar(100) not null  comment '',
   estado_rol           int(2) not null  comment '1 - activo
             2 - inactivo',
   primary key (id_rol)
);

/*==============================================================*/
/* Table: tickets                                               */
/*==============================================================*/
create table tickets
(
   id_ticket            bigint not null auto_increment  comment '',
   id_categoria         bigint not null  comment '',
   id_cliente           bigint not null  comment '',
   hora_ticket          time not null  comment '',
   fecha_ticket         date not null  comment '',
   asunto_ticket        varchar(200) not null  comment '',
   descripcion_ticket   text not null  comment '',
   numero_ticket        bigint not null  comment '',
   estado_ticket        int(2) not null  comment '1 - abierto
             2 - cerrado
             3 - proceso
             4 - cancelado',
   primary key (id_ticket)
);

/*==============================================================*/
/* Table: usuarios                                              */
/*==============================================================*/
create table usuarios
(
   id_usuario           bigint not null auto_increment  comment '',
   id_rol               bigint not null  comment '',
   nombres_usuario      varchar(200) not null  comment '',
   celular_usuario      bigint not null  comment '',
   correo_usuario       varchar(200) not null  comment '',
   direccion_usuario    text not null  comment '',
   usuario_acceso       varchar(200) not null  comment '',
   clave_acceso         varchar(200) not null  comment '',
   estado_usuario       int(2) not null  comment '1 - activo
             2 - inactivo',
   primary key (id_usuario)
);

alter table imagenes_respuestas add constraint fk_imagenes_reference_respuest foreign key (id_respuesta_ticket)
      references respuestas_tickets (id_respuesta_ticket) on delete cascade on update cascade;

alter table imagenes_tickets add constraint fk_imagenes_reference_tickets foreign key (id_ticket)
      references tickets (id_ticket) on delete cascade on update cascade;

alter table respuestas_tickets add constraint fk_respuest_reference_tickets foreign key (id_ticket)
      references tickets (id_ticket) on delete cascade on update cascade;

alter table respuestas_tickets add constraint fk_respuest_reference_usuarios foreign key (id_usuario)
      references usuarios (id_usuario) on delete cascade on update cascade;

alter table tickets add constraint fk_tickets_reference_categori foreign key (id_categoria)
      references categorias (id_categoria) on delete cascade on update cascade;

alter table tickets add constraint fk_tickets_reference_usuarios foreign key (id_cliente)
      references usuarios (id_usuario) on delete cascade on update cascade;

alter table usuarios add constraint fk_usuarios_reference_roles foreign key (id_rol)
      references roles (id_rol) on delete cascade on update cascade;

